(function(){
	if (!('serviceWorker' in navigator)) {
    // Service Worker isn't supported on this browser, disable or hide UI.
    return;
  }

  const options = {
		userVisibleOnly: true,
    applicationServerKey: urlBase64ToUint8Array(
      'BHQO2qTdLaOfXb-QSlTcXcWFmuvp7cAR_KREquMHserCyd_Sqql8ExkCeiRRt6Alr5LuAvdr0fhwIWKAqx1aSTA'
    )
	};


  window.swRegistration = navigator.serviceWorker.register('/sw');

  window.unsubscribe = function(){
  	if(!('swRegistration' in window)) return;
  	let getSubscriptionPr = 
  		window.swRegistration.then(registration =>{
  			console.log(">:(")
  			return registration.pushManager.getSubscription();
  		});

  	let unsubscribeUserPr =  getSubscriptionPr.then(subs => {
				// Returns promise
				console.log(subs);
				if(subs) return subs.unsubscribe();
				else Promise.reject("No subscription");

			})
			.catch(err =>{
				console.log("Could not unsubscribe");
				console.log(err);
			});

		return Promise.all([getSubscriptionPr,unsubscribeUserPr]);

  };

  window.hasPermission = function(){
  	console.log("Called has permission");
  	if(!('swRegistration' in window)) return Promise.resolve(false);
  	console.log(":/");
  	let getStatePr = 
  		window.swRegistration.then(registration =>{
  			console.log(">:(")
  			return registration.pushManager.getSubscription();
  		});

  	return getStatePr.then(sub =>{
  		return sub != null;
  	}).catch(console.log);
  }

  window.requestPushNotifications = function(){
  	if(!('swRegistration' in window)) return;

  	return window.swRegistration.then(registration=>{
			
			return registration.pushManager.subscribe(options);
		})
		.catch(console.log);

  }

  function urlBase64ToUint8Array(base64String) {
	  const padding = '='.repeat((4 - base64String.length % 4) % 4);
	  const base64 = (base64String + padding)
	    .replace(/\-/g, '+')
	    .replace(/_/g, '/');
	 
	  const rawData = window.atob(base64);
	  const outputArray = new Uint8Array(rawData.length);
	 
	  for (let i = 0; i < rawData.length; ++i) {
	    outputArray[i] = rawData.charCodeAt(i);
	  }
	  return outputArray;
	}

	function requestPushNotificationPermission(){
  	if (!('PushManager' in window)) {
	    // Push isn't supported on this browser, disable or hide UI.
	    return;
	  }
  	return new Promise(function(resolve,reject){
  		const permissionRequest = Notification.requestPermission(function(result){
  			resolve(result);
  		});

  		if(permissionRequest)
  			permissionRequest.then(resolve,reject);
  	});
  }
})();