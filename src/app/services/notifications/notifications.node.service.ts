import { Injectable } from '@angular/core';
import { NotificationService} from './notifications.abstract.service';

@Injectable()
export class NotificationServiceNode implements NotificationService{
	requestPermission(){}
	saveSubscription(data){}
	isPermitted() : Promise<any>{
		return Promise.resolve(false);
	}
	removePermission(){}
	togglePermission(){}
}